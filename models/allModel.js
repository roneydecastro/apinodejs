var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var allSchema = new Schema({
    username: String,
    full_name: String,
    isDone: Boolean,
    hasAttachment: Boolean
});

var all = mongoose.model('All', allSchema);

module.exports = all;