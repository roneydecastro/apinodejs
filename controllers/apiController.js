var All = require('../models/allModel');
var bodyParser = require('body-parser');

module.exports = function(app) {
    
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    
    app.get('/api/all/:uname', function(req, res) {

        All.find({ username: req.params.uname }, function(err, all) {
            if (err) throw err;
            
            res.send(all);
        });
        
    });
    
    app.get('/api/all/:id', function(req, res) {

        All.findById({ _id: req.params.id }, function(err, user) {
           if (err) throw err;
           
           res.send(user);
       });
        
    });
    
    app.post('/api/all', function(req, res) {
        
        if (req.body.id) {
            All.findByIdAndUpdate(req.body.id, { full_name: req.body.full_name, isDone: req.body.isDone, hasAttachment: req.body.hasAttachment }, function(err, user) {
                if (err) throw err;
                
                res.send('Success');
            });
        }
        
        else {
           
           var newUser = All({
               username: 'test',
               full_name: req.body.full_name,
               isDone: req.body.isDone,
               hasAttachment: req.body.hasAttachment
           });
            newUser.save(function(err) {
               if (err) throw err;
               res.send('Success');
           });
            
        }
        
    });
    
    app.delete('/api/todo', function(req, res) {

        All.findByIdAndRemove(req.body.id, function(err) {
            if (err) throw err;
            res.send('Success');
        })
        
    });
    
}