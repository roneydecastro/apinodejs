var All = require('../models/allModel');

module.exports = function(app) {
    
   app.get('/api/setupAll', function(req, res) {
       
       // seed database
       var starterAll = [
           {
               username: 'test',
               full_name: 'User Test',
               isDone: false,
               hasAttachment: false
           },
           {
               username: 'test_two',
               full_name: 'User Test Two',
               isDone: false,
               hasAttachment: false
           },
           {
               username: 'test_three',
               full_name: 'User Test Three',
               isDone: false,
               hasAttachment: false
           }
       ];
       All.create(starterAll, function(err, results) {
           res.send(results);
       }); 
   });
    
}